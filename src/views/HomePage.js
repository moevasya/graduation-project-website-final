import { React } from "react";
import styled from "styled-components";
import Amman from "../images/amman.jpeg";
import Carousel from "react-bootstrap/Carousel";
import Navbar from "../components/Navbar.js";
import Footer from "../components/Footer.js";
import Clock from "react-live-clock";
import Loading from "../components/Loading";

import { Link } from "react-router-dom";

export default function HomePage(props) {
	const News = props.API;
	return News ? (
		<Wrapper>
			<Navbar />

			<LandingImage>
				<Info>Amman Jordan</Info>
				<ClockContainer>
					<Clock format={"HH:mm:ss"} ticking={true} timezone={"Amman"} />
				</ClockContainer>
			</LandingImage>
			<Carousel className="Carousel" fade={true} style={{ "z-index": "0" }}>
				<Carousel.Item interval={300000}>
					<img
						className="d-block w-100 h-90 "
						src={News.articles["0"].urlToImage}
						alt="First slide"
					/>
					<Mask>
						<Carousel.Caption>
							<Link to="/News0" className="CarouselLinks">
								<h5>{News.articles["0"].title}</h5>
								<p>{News.articles["0"].description}</p>
							</Link>
						</Carousel.Caption>
					</Mask>
				</Carousel.Item>

				<Carousel.Item interval={300000}>
					<img
						className="d-block w-100 h-90"
						src={News.articles["1"].urlToImage}
						alt="Third slide"
					/>
					<Mask>
						<Carousel.Caption>
							<Link to="/News1" className="CarouselLinks">
								<h5>{News.articles["1"].title}</h5>
								<p>{News.articles["1"].description}</p>
							</Link>
						</Carousel.Caption>
					</Mask>
				</Carousel.Item>
				<Carousel.Item interval={300000}>
					<img
						className="d-block w-100 h-90"
						src={News.articles["2"].urlToImage}
						alt="Third slide"
					/>
					<Mask>
						<Carousel.Caption>
							<Link to="/News2" className="CarouselLinks">
								<h5>{News.articles["2"].title}</h5>
								<p>{News.articles["2"].description}</p>
							</Link>
						</Carousel.Caption>
					</Mask>
				</Carousel.Item>
				<Carousel.Item interval={300000}>
					<img
						className="d-block w-100 h-90"
						src={News.articles["3"].urlToImage}
						alt="Third slide"
					/>
					<Mask>
						<Carousel.Caption>
							<Link to="/News3" className="CarouselLinks">
								<h5>{News.articles["3"].title}</h5>
								<p>{News.articles["3"].description}</p>
							</Link>
						</Carousel.Caption>
					</Mask>
				</Carousel.Item>
				<Carousel.Item interval={300000}>
					<img
						className="d-block w-100 h-90"
						src={News.articles["4"].urlToImage}
						alt="Third slide"
					/>
					<Mask>
						<Carousel.Caption>
							<Link to="/News4" className="CarouselLinks">
								<h5>{News.articles["4"].title}</h5>
								<p>{News.articles["4"].description}</p>
							</Link>
						</Carousel.Caption>
					</Mask>
				</Carousel.Item>
				<Carousel.Item interval={300000}>
					<img
						className="d-block w-100 h-90"
						src={News.articles["5"].urlToImage}
						alt="Third slide"
					/>
					<Mask>
						<Carousel.Caption>
							<Link to="/News5" className="CarouselLinks">
								<h5>{News.articles["5"].title}</h5>
								NewsNews			<p>{News.articles["5"].description}</p>
							</Link>
						</Carousel.Caption>
					</Mask>
				</Carousel.Item>
				<Carousel.Item interval={300000}>
					<img
						className="d-block w-100 h-90"
						src={News.articles["6"].urlToImage}
						alt="Third slide"
					/>
					<Mask>
						<Carousel.Caption>
							<Link to="/News6" className="CarouselLinks">
								<h5>{News.articles["6"].title}</h5>
								<p>{News.articles["6"].description}</p>
							</Link>
						</Carousel.Caption>
					</Mask>
				</Carousel.Item>
				<Carousel.Item interval={300000}>
					<img
						className="d-block w-100 h-90"
						src={News.articles["7"].urlToImage}
						alt="Third slide"
					/>
					<Mask>
						<Carousel.Caption>
							<Link to="/News7" className="CarouselLinks">
								<h5>{News.articles["7"].title}</h5>
								<p>{News.articles["7"].description}</p>
							</Link>
						</Carousel.Caption>
					</Mask>
				</Carousel.Item>
				<Carousel.Item interval={300000}>
					<img
						className="d-block w-100 h-90"
						src={News.articles["8"].urlToImage}
						alt="Third slide"
					/>
					<Mask>
						<Carousel.Caption>
							<Link to="/News8" className="CarouselLinks">
								<h5>{News.articles["8"].title}</h5>
								<p>{News.articles["8"].description}</p>
							</Link>
						</Carousel.Caption>
					</Mask>
				</Carousel.Item>
				<Carousel.Item interval={300000}>
					<img
						className="d-block w-100 h-90"
						src={News.articles["9"].urlToImage}
						alt="Third slide"
					/>
					<Mask>
						<Carousel.Caption>
							<Link to="/News9" className="CarouselLinks">
								<h5>{News.articles["9"].title}</h5>
								<p>{News.articles["9"].description}</p>
							</Link>
						</Carousel.Caption>
					</Mask>
				</Carousel.Item>
				<Carousel.Item interval={300000}>
					<img
						className="d-block w-100 h-90"
						src={News.articles["10"].urlToImage}
						alt="Third slide"
					/>
					<Mask>
						<Carousel.Caption>
							<Link to="/News10" className="CarouselLinks">
								<h5>{News.articles["10"].title}</h5>
								<p>{News.articles["10"].description}</p>
							</Link>
						</Carousel.Caption>
					</Mask>
				</Carousel.Item>
				<Carousel.Item interval={300000}>
					<img
						className="d-block w-100 h-90"
						src={News.articles["11"].urlToImage}
						alt="Third slide"
					/>
					<Mask>
						<Carousel.Caption>
							<Link to="/News11" className="CarouselLinks">
								<h5>{News.articles["11"].title}</h5>
								<p>{News.articles["11"].description}</p>
							</Link>
						</Carousel.Caption>
					</Mask>
				</Carousel.Item>
			</Carousel>
			<Footer page={"Home"} />
		</Wrapper>
	) : (
		<Loading />
	);
}
const Wrapper = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
`;
const LandingImage = styled.div`
	width: 100%;
	height: 768px;
	background: url(${Amman});
	background-repeat: no-repeat;
	background-size: cover;
`;
const Info = styled.div`
	position: absolute;
	left: 60.74%;
	right: 2.55%;
	top: 24.17%;
	bottom: 56.25%;

	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 35px;
	display: flex;
	align-items: flex-start;
	text-align: center;
	background: rgba(255, 255, 255, 0.2);
	backdrop-filter: blur(25px);
	/* white */
	border-radius: 30px;
	/* white */
	width: fit-content;
	padding: 20px;
	color: #ffffff;
	@media (max-width: 1100px) {
		font-size: 25px;
	}
	@media (max-width: 768px) {
		left: 10%;
	}
`;
const ClockContainer = styled.div`
	position: absolute;
	left: 64.74%;
	right: 2.55%;
	top: 40%;
	bottom: 56.25%;
	padding: 20px;
	padding-top: 0px;
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 35px;
	display: flex;
	align-items: center;
	text-align: center;

	/* white */

	color: #ffffff;
	@media (max-width: 1100px) {
		font-size: 25px;
	}
	@media (max-width: 768px) {
		left: 10%;
	}
`;

const Mask = styled.div`
	width: 100%;
	height: 18%;
	background-color: rgba(0, 0, 0, 0.6);
	top: 0;
	vertical-align: top;
`;
