import React from "react";
import Navbar from "../components/Navbar.js";
import styled from "styled-components";
import Carousel from "react-bootstrap/Carousel";
import { useParams } from "react-router-dom";
import Footer from "../components/Footer.js";
import { Link } from "react-router-dom";
import Loading from "../components/Loading.js";
import { scrollTop } from "../App.js";
export default function News(props) {
	const News = props.API;
	const { id } = useParams();
	return News ? (
		<>
			<MainCont>
				<Navbar />
				<PageTitleContainer>
					<PageTitle>News</PageTitle>
				</PageTitleContainer>
				<TopDivider />
				<HeadLineDiv>
					<HeadLine>{News.articles[id].title}</HeadLine>
				</HeadLineDiv>
				<ImageContainer>
					<Image src={News.articles[id].urlToImage}></Image>
				</ImageContainer>
				<DetailsContainer>
					<Details>
						source: {News.articles[id].source.name}
						<br></br>
						<br></br>
						{News.articles[id].description} <b></b>{" "}
						<a
							className="Link"
							style={{
								color: "#54416d",
								border: "1px solid #54416d",
								padding: "5px 20px",
								borderRadius: "30px",
								margin: "15px auto",
							}}
							target="_blank"
							href={News.articles[id].url}
						>
							Continue Reading
						</a>
					</Details>
				</DetailsContainer>
				<CaroDiv>
					<Carousel className="Carousel" fade={true}>
						<Carousel.Item interval={300000}>
							<img
								className="d-block w-100 h-90 "
								src={News.articles["0"].urlToImage}
								alt="First slide"
							/>
							<Mask>
								<Carousel.Caption>
									<Link
										to="/News0"
										className="CarouselLinks"
										onClick={scrollTop}
									>
										<h5>{News.articles["0"].title}</h5>
										<p>{News.articles["0"].description}</p>
									</Link>
								</Carousel.Caption>
							</Mask>
						</Carousel.Item>

						<Carousel.Item interval={300000}>
							<img
								className="d-block w-100 h-90"
								src={News.articles["1"].urlToImage}
								alt="Third slide"
							/>
							<Mask>
								<Carousel.Caption>
									<Link to="/News1" className="CarouselLinks">
										<h5>{News.articles["1"].title}</h5>
										<p>{News.articles["1"].description}</p>
									</Link>
								</Carousel.Caption>
							</Mask>
						</Carousel.Item>
						<Carousel.Item interval={300000}>
							<img
								className="d-block w-100 h-90"
								src={News.articles["2"].urlToImage}
								alt="Third slide"
							/>
							<Mask>
								<Carousel.Caption>
									<Link to="/News2" className="CarouselLinks">
										<h5>{News.articles["2"].title}</h5>
										<p>{News.articles["2"].description}</p>
									</Link>
								</Carousel.Caption>
							</Mask>
						</Carousel.Item>
						<Carousel.Item interval={300000}>
							<img
								className="d-block w-100 h-90"
								src={News.articles["3"].urlToImage}
								alt="Third slide"
							/>
							<Mask>
								<Carousel.Caption>
									<Link to="/News3" className="CarouselLinks">
										<h5>{News.articles["3"].title}</h5>
										<p>{News.articles["3"].description}</p>
									</Link>
								</Carousel.Caption>
							</Mask>
						</Carousel.Item>
						<Carousel.Item interval={300000}>
							<img
								className="d-block w-100 h-90"
								src={News.articles["4"].urlToImage}
								alt="Third slide"
							/>
							<Mask>
								<Carousel.Caption>
									<Link to="/News4" className="CarouselLinks">
										<h5>{News.articles["4"].title}</h5>
										<p>{News.articles["4"].description}</p>
									</Link>
								</Carousel.Caption>
							</Mask>
						</Carousel.Item>
						<Carousel.Item interval={300000}>
							<img
								className="d-block w-100 h-90"
								src={News.articles["5"].urlToImage}
								alt="Third slide"
							/>
							<Mask>
								<Carousel.Caption>
									<Link to="/News5" className="CarouselLinks">
										<h5>{News.articles["5"].title}</h5>
										<p>{News.articles["5"].description}</p>
									</Link>
								</Carousel.Caption>
							</Mask>
						</Carousel.Item>
						<Carousel.Item interval={300000}>
							<img
								className="d-block w-100 h-90"
								src={News.articles["6"].urlToImage}
								alt="Third slide"
							/>
							<Mask>
								<Carousel.Caption>
									<Link to="/News6" className="CarouselLinks">
										<h5>{News.articles["6"].title}</h5>
										<p>{News.articles["6"].description}</p>
									</Link>
								</Carousel.Caption>
							</Mask>
						</Carousel.Item>
						<Carousel.Item interval={300000}>
							<img
								className="d-block w-100 h-90"
								src={News.articles["7"].urlToImage}
								alt="Third slide"
							/>
							<Mask>
								<Carousel.Caption>
									<Link to="/News7" className="CarouselLinks">
										<h5>{News.articles["7"].title}</h5>
										<p>{News.articles["7"].description}</p>
									</Link>
								</Carousel.Caption>
							</Mask>
						</Carousel.Item>
						<Carousel.Item interval={300000}>
							<img
								className="d-block w-100 h-90"
								src={News.articles["8"].urlToImage}
								alt="Third slide"
							/>
							<Mask>
								<Carousel.Caption>
									<Link to="/News8" className="CarouselLinks">
										<h5>{News.articles["8"].title}</h5>
										<p>{News.articles["8"].description}</p>
									</Link>
								</Carousel.Caption>
							</Mask>
						</Carousel.Item>
						<Carousel.Item interval={300000}>
							<img
								className="d-block w-100 h-90"
								src={News.articles["9"].urlToImage}
								alt="Third slide"
							/>
							<Mask>
								<Carousel.Caption>
									<Link to="/News9" className="CarouselLinks">
										<h5>{News.articles["9"].title}</h5>
										<p>{News.articles["9"].description}</p>
									</Link>
								</Carousel.Caption>
							</Mask>
						</Carousel.Item>
						<Carousel.Item interval={300000}>
							<img
								className="d-block w-100 h-90"
								src={News.articles["10"].urlToImage}
								alt="Third slide"
							/>
							<Mask>
								<Carousel.Caption>
									<Link to="/News10" className="CarouselLinks">
										<h5>{News.articles["10"].title}</h5>
										<p>{News.articles["10"].description}</p>
									</Link>
								</Carousel.Caption>
							</Mask>
						</Carousel.Item>
						<Carousel.Item interval={300000}>
							<img
								className="d-block w-100 h-90"
								src={News.articles["11"].urlToImage}
								alt="Third slide"
							/>
							<Mask>
								<Carousel.Caption>
									<Link to="/News11" className="CarouselLinks">
										<h5>{News.articles["11"].title}</h5>
										<p>{News.articles["11"].description}</p>
									</Link>
								</Carousel.Caption>
							</Mask>
						</Carousel.Item>
					</Carousel>
				</CaroDiv>
				<Footer page={"News"} />
			</MainCont>
		</>
	) : (
		<Loading />
	);
}
const CaroDiv = styled.div`
	width: 100%;
`;

const Mask = styled.div`
	width: 100%;
	height: 18%;
	background-color: rgba(0, 0, 0, 0.6);

	vertical-align: top;
`;
const MainCont = styled.div`
	display: flex;
	flex-direction: column;
	padding: 50px 0 0;
	justify-content: center;
`;
const PageTitleContainer = styled.div`
	width: 201px;
	left: 65px;
	top: 117px;
	margin-left: 5%;
`;

const PageTitle = styled.h1`
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 40px;
	line-height: 94px;
	display: flex;
	align-items: center;

	color: #2b235a;
	@media (max-with: 1000px) {
		font-size: 25px;
	}
	@media (max-with: 550px) {
		font-size: 20px;
	}
`;
const TopDivider = styled.div`
	width: 90%;
	height: 0px;
	align-self: center;

	border: 2px solid #2b235a;
`;

const ImageContainer = styled.div`
	width: 100%;
	margin: 0px auto;
	display: flex;
	justify-content: center;
`;
const Image = styled.img`
	width: 90%;
	height: 600px;
	align-self: center;
`;
const HeadLineDiv = styled.div`
	width: 60%;
	margin: 20px 5% 0;

	background: #ffffff;
`;

const HeadLine = styled.p`
	left: 0%;
	right: 0%;
	top: 0%;
	bottom: 0%;

	font-family: Roboto;
	font-style: normal;
	font-weight: bold;
	font-size: 25px;
	display: flex;
	align-items: center;

	/* black */

	color: #000000;
	@media (max-width: 1000px) {
		font-size: 20px;
	}
`;
const DetailsContainer = styled.div`
	width: 100%;
	height: auto;
	left: 65px;
	top: 1000px;
	display: flex;
	flex-direction: column;
	padding: 20px 5%;
`;
const Details = styled.div`
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 20px;
	display: flex;
	align-items: center;
	display: flex;
	flex-direction: column;
	color: #000000;
	@media (max-width: 768px) {
		font-size: 15px;
	}
`;
