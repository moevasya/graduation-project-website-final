import React from "react";
import styled from "styled-components";
import Navbar from "../components/Navbar.js";
import Footer from "../components/Footer.js";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import axios from "axios";
import Loading from "../components/Loading.js";
import { scrollTop } from "../App.js";
export default function Tour() {
	const [info, getInfo] = useState("");
	const [Image, setImage] = useState("");

	const { sightProp } = useParams();
	const inquire = {
		name: sightProp,
	};
	useEffect(() => {
		axios
			.post(
				"https://graduation-project-server-final.vercel.app/app/get",
				inquire,
			)
			.then((response) => {
				getInfo(response.data);
				setImage(response.data.image);
			})
			.catch((error) => console.error(`Error: ${error}`));
	}, [sightProp]);
	return info ? (
		<Wrapper>
			<Navbar />
			<ImageCont style={{ backgroundImage: `url(${Image})` }}>
				<GlassMorph>
					<Name>{info.name}</Name>
				</GlassMorph>
			</ImageCont>
			<Description>{info.description}</Description>
			<Name>More Sights</Name>

			<Container>
				<Link className={"align link"} to={"/Tour/Petra"} onClick={scrollTop}>
					{" "}
					<Sight>Petra</Sight>
				</Link>
				<Link className={"align link"} to={"/Tour/Al-Siq"} onClick={scrollTop}>
					{" "}
					<Sight>Al-Siq</Sight>
				</Link>
				<Link
					className={"align link"}
					to={"/Tour/Dana Reserve"}
					onClick={scrollTop}
				>
					{" "}
					<Sight>Dana Reserve</Sight>
				</Link>
				<Link
					className={"align link"}
					to={"/Tour/Al-Karak Castle"}
					onClick={scrollTop}
				>
					{" "}
					<Sight>Al-Karak Castle</Sight>
				</Link>
				<Link
					className={"align link"}
					to={"/Tour/Temple of Hercules"}
					onClick={scrollTop}
				>
					{" "}
					<Sight>Temple of Hercules</Sight>
				</Link>
				<Link
					className={"align link"}
					to={"/Tour/Mount Nebo"}
					onClick={scrollTop}
				>
					{" "}
					<Sight>Mount Nebo</Sight>
				</Link>
				<Link
					className={"align link"}
					to={"/Tour/Wadi Rum"}
					onClick={scrollTop}
				>
					{" "}
					<Sight>Wadi Rum</Sight>
				</Link>
				<Link
					className={"align link"}
					to={"/Tour/Ayla Oasis"}
					onClick={scrollTop}
				>
					{" "}
					<Sight>Ayla Oasis</Sight>
				</Link>
				<Link
					className={"align link"}
					to={"/Tour/Ajloun Reserve"}
					onClick={scrollTop}
				>
					{" "}
					<Sight>Ajloun Reserve</Sight>
				</Link>{" "}
			</Container>
			<Footer page={"Tour"} />
		</Wrapper>
	) : (
		<Loading />
	);
}
const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	width: 100%;
`;
const ImageCont = styled.div`
	width: 100%;
	height: 644px;
	background-size: cover;
	background-repeat: no-repeat;
`;

const GlassMorph = styled.div`
	position: absolute;
	left: 2.72%;
	right: 74.41%;
	top: 17.04%;
	bottom: 78.52%;
	width: 500px;
	height: 100px;
	background: rgba(255, 255, 255, 0.1);
	backdrop-filter: blur(25px);
	/* Note: backdrop-filter has minimal browser support */
	display: flex;
	border-radius: 15px;
	@media (max-width: 1000px) {
		width: 250px;
	}
`;

const Name = styled.div`
	justify-content: center;

	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 55px;
	line-height: 88px;
	display: flex;
	align-items: center;
	text-align: center;
	width: 100%;
	color: #54416d;
	@media (max-width: 1000px) {
		font-size: 25px;
	}
`;

const Description = styled.div`
	width: 95%;
	height: auto;
	margin: 20px;
	padding: 10px;
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 25px;
	line-height: 47px;
	display: flex;
	align-items: center;
	border-radius: 20px;
	border: 2px solid #54416d;
	color: #000000;
	align-self: center;
	@media (max-width: 1000px) {
		font-size: 15px;
	}
`;
const Container = styled.div`
	width: 100%;
	height: auto;
	margin: 50px 0;
	background: #75b4e3;
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	justify-content: center;
`;
const Sight = styled.h1`
	font-size: 35px;
	color: white;
	margin: 40px 5%;
	text-align: center;
	height: 50px;
	transition: all 0.3s ease;
	@media (max-width: 1000px) {
		font-size: 20px;
		margin: 20px 5%;
	}
	@media (max-width: 400px) {
		font-size: 15px;
		margin: 10px 5%;
	}
	&:hover {
		color: #54416d;
	}
`;
