import { React, useState, useEffect } from "react";
import styled from "styled-components";
import Navbar from "../components/Navbar.js";
import Footer from "../components/Footer.js";
import axios from "axios";
import Sky from "../images/sky.jpg";
import { Link, useParams } from "react-router-dom";
import Loading from "../components/Loading";
import { scrollTop } from "../App.js";
export default function Weather() {
	const { id } = useParams();

	const [weatherAPI, getWeather] = useState("");

	useEffect(() => {
		axios
			.get(
				`https://graduation-project-server-final.vercel.app/app/weather?address=${id}`,
			)
			.then((response) => {
				getWeather(response.data);
			})
			.catch((error) => console.error(`Error: ${error}`));
	}, [id]);
	return weatherAPI ? (
		<>
			<Navbar />
			<MainCont>
				<FirstTitleContainer>
					<FirstTitle>
						{" "}
						Current Weather Condition in {weatherAPI.city["name"]}
					</FirstTitle>
					<FirstDivider></FirstDivider>
					<FirstContainer>
						<FirstImage>
							<FirstGlassEffect>
								<Row>
									<CityName>
										{weatherAPI.city["name"]}, {weatherAPI.city.country}
									</CityName>
									<Temp>
										{" "}
										{Math.trunc(weatherAPI.list["0"].main["temp"] - 270)}&#176;
									</Temp>
								</Row>
								<Row style={{ justifyContent: "center" }}>
									<Icon>
										<img
											alt=""
											className=""
											src={`https://openweathermap.org/img/wn/${weatherAPI.list["0"].weather["0"].icon}.png`}
										></img>
										<h5>{weatherAPI.list["0"].weather["0"].main}</h5>
									</Icon>
								</Row>
							</FirstGlassEffect>
						</FirstImage>

						<FirstSubCont>
							<table class="Table">
								<tbody>
									<tr>
										<th className="TableElements">Temp</th>
										<th className="TableElements">
											{Math.trunc(weatherAPI.list["0"].main["temp"] - 270)}
											&#176;
										</th>
										<th className="TableElements">Real Feel</th>
										<th className="TableElements">
											{Math.trunc(
												weatherAPI.list["0"].main["feels_like"] - 270,
											)}
											&#176;
										</th>
									</tr>

									<tr>
										<th className="TableElements">Ground Level</th>
										<th className="TableElements">
											{weatherAPI.list["0"].main["grnd_level"]}
										</th>
										<th className="TableElements">Pressure</th>
										<th className="TableElements">
											{weatherAPI.list["0"].main["pressure"]}
										</th>
									</tr>
									<tr>
										<th className="TableElements">Max Tempreture</th>
										<th className="TableElements">
											{Math.trunc(weatherAPI.list["0"].main["temp_max"] - 270)}
											&#176;
										</th>
										<th className="TableElements">Min Tempreture</th>
										<th className="TableElements">
											{Math.trunc(weatherAPI.list["0"].main["temp_min"] - 270)}
											&#176;
										</th>
									</tr>
									<tr>
										<th className="TableElements">Humidity</th>
										<th className="TableElements">
											{weatherAPI.list["0"].main["humidity"]}%
										</th>
										<th className="TableElements">Description</th>
										<th className="TableElements">
											{weatherAPI.list["0"].weather["0"].description}
										</th>
									</tr>
								</tbody>
							</table>
						</FirstSubCont>
					</FirstContainer>
				</FirstTitleContainer>
				<SecondTitle>
					Extended Forecast in {weatherAPI.city["name"]}
				</SecondTitle>
				<SecondDivider></SecondDivider>
				<SecondContainer>
					<SecondGlassEffect>
						<SecondSubCont>
							<IconContainer>
								{(weatherAPI.list["8"].dt_txt + "").substring(0, 11)}
								<ExtendIcon
									src={`https://openweathermap.org/img/wn/${weatherAPI.list["8"].weather["0"].icon}.png`}
								></ExtendIcon>
								{weatherAPI.list["8"].weather["0"].description}
								<br></br>
								{Math.trunc(weatherAPI.list["8"].main["temp"] - 270)}&#176;
							</IconContainer>
							<IconContainer>
								{(weatherAPI.list["17"].dt_txt + "").substring(0, 11)}
								<ExtendIcon
									src={`https://openweathermap.org/img/wn/${weatherAPI.list["16"].weather["0"].icon}.png`}
								></ExtendIcon>
								{weatherAPI.list["17"].weather["0"].description}
								<br></br>
								{Math.trunc(weatherAPI.list["17"].main["temp"] - 270)}&#176;
							</IconContainer>
							<IconContainer>
								{(weatherAPI.list["25"].dt_txt + "").substring(0, 11)}
								<ExtendIcon
									src={`https://openweathermap.org/img/wn/${weatherAPI.list["24"].weather["0"].icon}.png`}
								></ExtendIcon>
								{weatherAPI.list["25"].weather["0"].description}
								<br></br>
								{Math.trunc(weatherAPI.list["25"].main["temp"] - 270)}&#176;
							</IconContainer>
						</SecondSubCont>
					</SecondGlassEffect>
				</SecondContainer>
				<ThirdTitle>City List</ThirdTitle>
				<ThirdDivider></ThirdDivider>
				<ThirdContainer>
					<table class="Table">
						<tbody>
							<br></br>

							<tr>
								<Link
									className="TableElements Align"
									to={"/WeatherAmman"}
									onClick={scrollTop}
								>
									<td className="TextAlign">Amman</td>
								</Link>
								<Link
									className="TableElements Align"
									to={"/WeatherIrbid"}
									onClick={scrollTop}
								>
									<td className="TextAlign">Irbid</td>
								</Link>
								<Link
									className="TableElements Align"
									to={"/WeatherPetra"}
									onClick={scrollTop}
								>
									<td className="TextAlign">Petra </td>
								</Link>
								<Link
									className="TableElements Align"
									to={"/WeatherSalt"}
									onClick={scrollTop}
								>
									<td className="TextAlign">Salt</td>
								</Link>
							</tr>

							<br></br>

							<tr>
								<Link
									className="TableElements Align"
									to={"/WeatherAqaba"}
									onClick={scrollTop}
								>
									<td className="TextAlign">Aqaba</td>
								</Link>
								<Link
									className="TableElements Align"
									to={"/WeatherJerash"}
									onClick={scrollTop}
								>
									<td className="TextAlign">Jerash</td>
								</Link>
								<Link
									className="TableElements Align"
									to={"/WeatherZarqa"}
									onClick={scrollTop}
								>
									<td className="TextAlign">Zarqa</td>
								</Link>
								<Link
									className="TableElements Align"
									to={"/WeatherMafraq"}
									onClick={scrollTop}
								>
									<td className="TextAlign">Mafraq</td>
								</Link>
							</tr>
							<br></br>

							<tr>
								<Link
									className="TableElements Align"
									to={"/WeatherAdjlun"}
									onClick={scrollTop}
								>
									<td className="TextAlign">Ajlun</td>
								</Link>
								<Link
									className="TableElements Align"
									to={"/WeatherKarak"}
									onClick={scrollTop}
								>
									<td className="TextAlign">Karak</td>
								</Link>
								<Link
									className="TableElements Align"
									to={"/WeatherAl Tafile"}
									onClick={scrollTop}
								>
									<td className="TextAlign">Al Tafile</td>
								</Link>
								<Link
									className="TableElements Align"
									to={"/WeatherMadaba"}
									onClick={scrollTop}
								>
									<td className="TextAlign">Madaba</td>
								</Link>
							</tr>
						</tbody>
					</table>
				</ThirdContainer>
			</MainCont>
			<Footer page={"Weather"} />
		</>
	) : (
		<Loading />
	);
}

const MainCont = styled.div`
	padding: 50px 40px 0;
	display: flex;
	flex-direction: column;
	width: 100%;
`;
const FirstTitleContainer = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
`;
const FirstTitle = styled.h1`
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 30px;
	line-height: 1.5;
	display: flex;
	align-items: center;
	padding: 40px 50px 0;
	color: #2b235a;
	@media (max-width: 1000px) {
		font-size: 20px;
		line-height: 1.1;
		padding: 10px 0;
	}
`;
const FirstDivider = styled.div`
	display: flex;
	width: 93%;
	height: 0px;
	top: 100px;
	align-self: center;
	border: 1px solid #2b235a;
	margin: 20px 0 50px;
`;

const FirstContainer = styled.div`
	width: 100%;
	left: -65px;
	top: 150px;

	border-radius: 15px;
	background-color: #dddddd;
`;
const FirstImage = styled.div`
	left: 0%;
	right: 0%;
	top: 0%;
	bottom: 0%;
	background: url(${Sky});
	background-size: cover;
	background-repeat: no-repeat;
	width: 100%;
	height: 500px;
	border-radius: 15px;
	@media (max-width: 1000px) {
		height: 380px;
	}
`;
const FirstGlassEffect = styled.div`
	display: flex;
	flex-direction: column;
	background: rgba(255, 255, 255, 0.2);
	backdrop-filter: blur(25px);
	/* Note: backdrop-filter has minimal browser support */
	height: 100%;
	border-radius: 15px;
`;
const Row = styled.div`
	display: flex;
	flex-direction: row;
	width: 100%;
	justify-content: space-between;
	padding: 50px;
	@media (max-width: 1000px) {
		justify-content: space-around;
		padding: 20px;
	}
`;

const FirstSubCont = styled.div`
	left: 0%;

	top: 411px;
	width: 100%;

	background: #ffffff;
	/* color three */

	border: 2px solid #75b4e3;
	box-sizing: border-box;
	border-radius: 15px;
`;
const CityName = styled.div`
	left: 10%;
	right: 35.97%;
	top: 5.64%;
	bottom: 74.29%;

	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 30px;
	line-height: 94px;
	display: flex;
	align-items: center;
	text-align: center;

	/* white */

	color: #ffffff;
	@media (max-width: 1000px) {
		font-size: 20px;
	}
`;

const Temp = styled.div`
	left: 75.62%;
	right: 1.53%;
	top: 1.22%;
	bottom: 69.87%;

	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 50px;
	line-height: 105px;
	display: flex;
	align-items: center;
	text-align: center;

	/* white */

	color: #ffffff;
	@media (max-width: 1000px) {
		font-size: 20px;
	}
`;

const Icon = styled.div`
	left: 45%;
	top: 30%;
	bottom: 63.77%;
	display: flex;
	flex-direction: column;
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 40px;
	line-height: 47px;
	display: flex;
	align-items: center;
	text-align: center;

	color: #ffffff;
`;

const SecondTitle = styled.div`
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 30px;
	line-height: 1.2;
	display: flex;
	align-items: center;
	padding: 20px 50px 0;
	margin: 20px 0;
	color: #2b235a;
	@media (max-width: 1000px) {
		font-size: 20px;
		line-height: 1.1;
		padding: 10px 0;
	}
`;
const SecondDivider = styled.div`
	width: 93%;
	height: 2px;
	left: 65px;
	top: 990px;
	align-self: center;
	border: 1px solid #2b235a;
	margin: 10px 0 50px;
`;
const SecondContainer = styled.div`
	width: 100%;
	left: 0px;
	top: 1050px;
	background: url(${Sky});
	background-repeat: no-repeat;
	background-size: cover;
	border-radius: 15px;
`;
const SecondGlassEffect = styled.div`
	display: flex;
	flex-direction: column;
	left: 0%;
	right: 0%;
	top: 0%;
	bottom: 0%;

	background: rgba(255, 255, 255, 0.2);
	backdrop-filter: blur(25px);
	/* Note: backdrop-filter has minimal browser support */

	border-radius: 15px;
`;

const SecondSubCont = styled.div`
	display: flex;
	flex-direction: row;
	width: 100%;
	margin: auto;
	text-align: center;
	min-height: 500px;
	align-items: center;
	justify-content: space-around;
	flex-wrap: wrap;
	padding: 20px;
`;
const ThirdTitle = styled.div`
	left: 67px;
	top: 1800px;
	bottom: 57.14%;

	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 30px;
	line-height: 1.2;
	display: flex;
	align-items: center;
	padding: 20px 50px;
	color: #2b235a;
	margin: 20px 0;
	@media (max-width: 1000px) {
		font-size: 20px;
		line-height: 1.1;
		padding: 10px 0;
	}
`;
const ThirdDivider = styled.div`
	width: 93%;
	align-self: center;
	height: 2px;
	left: 65px;
	top: 1840px;
	margin: 10px 0 40px;
	border: 1px solid #2b235a;
`;
const ThirdContainer = styled.div`
	width: 100%;
	height: auto;
	padding: 20px 20px 40px;
	background: #75b4e3;
	border-radius: 15px;
`;
const IconContainer = styled.div`
	width: 300px;
	height: 100%;
	display: flex;
	flex-direction: column;
	margin: 20px 5%;
	justify-content: center;
	text-align: center;
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 30px;
	color: #2b235a;
`;

const ExtendIcon = styled.img`
	height: 150px;
	width: 150px;
	margin: auto;
`;
