import { React, useState, useEffect } from "react";
import "./App.css";
import HomePage from "./views/HomePage.js";
import "bootstrap/dist/css/bootstrap.min.css";
import News from "./views/News";
import Weather from "./views/Weather.js";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import axios from "axios";
import Tour from "./views/Tour.js";
import NotFound from "./components/NotFound";
export const scrollTop = () => {
	window.scrollTo({ top: 0, behavior: "smooth" });
};
function App() {
	const url = `https://graduation-project-server-final.vercel.app/app/news`;
	const [newsApi, getNews] = useState("");

	useEffect(() => {
		axios
			.get(url)
			.then((response) => {
				getNews(response.data);
			})
			.catch((error) => console.error(`Error: ${error}`));
	}, [url]);

	return (
		<>
			<Router>
				<Switch>
					<Route exact path="/">
						<HomePage API={newsApi} />
					</Route>
					<Route path="/News:id">
						<News API={newsApi} />
					</Route>
					<Route path="/Weather:id">
						<Weather />
					</Route>
					<Route path="/Tour/:sightProp">
						<Tour />
					</Route>
					<Route
						path="*"
						render={() => (
							<>
								<NotFound />
							</>
						)}
					/>
				</Switch>
			</Router>
		</>
	);
}

export default App;
