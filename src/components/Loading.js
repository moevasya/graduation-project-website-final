import React from "react";
import styled from "styled-components";
import Navbar from "./Navbar";
import Footer from "./Footer";
import LoadingGif from "../images/loading.gif";
import { useHistory, Link } from "react-router";
export default function Loading() {
	const history = useHistory();
	return (
		<>
			<Navbar />
			<LoadingWrapper>
				<Container>
					<LoadingSymbol src={LoadingGif} />
					<Text>Fetching Your Data Please Wait ...</Text>
					<Text>Taking Too Long ? </Text>
					<Text
						onClick={() => {
							history.go(0);
						}}
						className="pointer"
					>
						Refresh Page{" "}
					</Text>
					<Text>Or</Text>
					<Text
						onClick={() => {
							history.push("/");
						}}
						className="pointer"
					>
						Go Back Home
					</Text>
				</Container>
			</LoadingWrapper>
			<Footer />
		</>
	);
}

const LoadingWrapper = styled.div`
	width: 100vw;
	height: 100vh;
	background: rgb(150, 124, 240);
	background: linear-gradient(
		90deg,
		rgba(150, 124, 240, 1) 0%,
		rgba(49, 37, 90, 1) 72%
	);
	display: flex;
	flex-direction: column;
	justify-content: center;
`;
const Container = styled.div`
	width: fit-content;
	height: fit-content;
	margin: auto;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
`;
const LoadingSymbol = styled.img`
	width: 10vw;
	height: 10vw;
`;
const Text = styled.p`
	color: white;
	font-size: 36px;
	text-align: center;
`;
