import React from "react";
import { StyledMenu } from "./Menu.styled";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { scrollTop } from "../../App";
const Menu = (open) => {
	return (
		<StyledMenu open={open.open}>
			<Link to="/" onClick={scrollTop}>
				<Home>Home</Home>
			</Link>
			<Link to="/News0" onClick={scrollTop}>
				<News>News</News>
			</Link>
			<Link to="/WeatherAmman" onClick={scrollTop}>
				<Weather>Weather</Weather>
			</Link>
			<Link to="/Tour/Petra" onClick={scrollTop}>
				<Tour>Tour</Tour>
			</Link>
		</StyledMenu>
	);
};
export default Menu;
const Home = styled.h2`
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 35px;
	line-height: 41px;
	display: flex;
	justify-content: flex-end;
	text-align: right;
	margin-right: 5%;
	transition: all 0.2s ease;
	margin-top: 20px;

	&:hover {
		margin-right: 8%;
		text-shadow: 0 0 25px #fff;
	}
	/* color 1 */

	color: #fff;
	@media (max-width: 1000px) {
		font-size: 25px;
		margin-right: 50px;
	}
`;

const News = styled.h2`
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 35px;
	line-height: 41px;
	display: flex;
	justify-content: flex-end;
	text-align: right;
	margin-right: 5%;
	transition: all 0.2s ease;
	margin-top: 20px;
	&:hover {
		margin-right: 8%;
		text-shadow: 0 0 25px #fff;
	}
	/* color 1 */

	color: #fff;
	@media (max-width: 1000px) {
		font-size: 25px;
		margin-right: 50px;
	}
`;
const Weather = styled.h2`
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 35px;
	line-height: 41px;
	display: flex;
	justify-content: flex-end;
	text-align: right;
	margin-right: 5%;
	/* color 1 */
	margin-top: 20px;

	color: #fff;
	transition: all 0.2s ease;

	&:hover {
		margin-right: 8%;
		text-shadow: 0 0 25px #fff;
	}
	@media (max-width: 1000px) {
		font-size: 25px;
		margin-right: 50px;
	}
`;

const Tour = styled.h2`
	font-family: Roboto;
	font-style: normal;
	font-weight: normal;
	font-size: 35px;
	line-height: 41px;
	display: flex;
	justify-content: flex-end;
	text-align: right;
	margin-right: 5%;
	transition: all 0.2s ease;
	margin-top: 20px;

	&:hover {
		margin-right: 8%;
		text-shadow: 0 0 25px #fff;
	}
	/* color 1 */

	color: #fff;
	@media (max-width: 1000px) {
		font-size: 25px;
		margin-right: 50px;
	}
`;
