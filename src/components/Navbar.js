import React, { useState } from "react";
import styled from "styled-components";
import Hamburger from "./hamburger/index.js";
import Drawer from "./menu/index.js";
import { Link } from "react-router-dom";
export default function Navbar() {
	const [open, setOpen] = useState(false);

	return (
		<NavbarDiv>
			<Link class="logo-link" to="/">
				<Title>Jordan Now</Title>
			</Link>
			<Hamburger open={open} setOpen={setOpen} />
			<Drawer open={open}></Drawer>
		</NavbarDiv>
	);
}

const NavbarDiv = styled.div`
	width: 100%;
	height: 50px;
	background-color: #ffffff;
	position: absolute;
	left: 0;
	top: 0;
	display: flex;
	flex-direction: row;
	border-bottom: 1px solid #2b235a;
	justify-content: space-between;
	position: fixed;
	top: 0;
	right: 0;
	left: 0;
	z-index: 100000;
`;

const Title = styled.h2`
	margin: 0 20px;
	width: auto;
	font-family: Roboto;
	font-style: normal;
	font-weight: Bolder;
	font-size: 30px;
	line-height: 41px;
	display: flex;
	align-items: center;
	z-index: 105;
	/* color 1 */
	transition: all 0.2s ease;
	&:hover {
		text-shadow: 0 0 20px #31255a;
	}
	color: #31255a;
	@media (max-width: 1100px) {
		font-size: 20px;
		line-height: 30px;
	}
`;
