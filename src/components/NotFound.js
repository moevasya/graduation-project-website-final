import React from "react";
import styled from "styled-components";
import NotFoundBg from "../images/404.jpg";
import Navbar from "./Navbar";
import Footer from "./Footer";
import { Link } from "react-router-dom";
export default function NotFound() {
	return (
		<>
			<Navbar />
			<Wrapper>
				<Column>
					<Phrase>Ooops!</Phrase>
					<Title>404</Title>
					<Subtitle>Looks like you went outside of our dimension ! </Subtitle>
					<Link to={"/"} className="nf-button">
						Take me Back Home !
					</Link>
				</Column>
			</Wrapper>
			<Footer />
		</>
	);
}
const Phrase = styled.div`
	font-size: 5vw;
	color: black;
	margin: 5px auto;
	color: #31255a;
	font-weight: 600;
`;

const Wrapper = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: flex-end;
	background: url(${NotFoundBg});
	background-size: contain;
	background-repeat: no-repeat;
	width: 100%;
	height: 100vh;
	background-position-x: -60%;
`;
const Column = styled.div`
	display: flex;
	flex-direction: column;
	height: 100%;
	width: 30%;
	padding: 100px 10px;
	margin-right: 3vw;
`;
const Title = styled.p`
	font-size: 10vw;
	color: black;
	margin: 5px auto;
	color: #31255a;
	font-weight: 600;
`;
const Subtitle = styled.p`
	font-size: 2vw;
	color: black;
	margin: 5px auto;
	text-align: center;
	color: #31255a;
`;
