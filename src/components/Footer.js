import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { scrollTop } from "../App";
export default function Footer(props) {
	var Cont = styled.div`
		width: 100%;
		height: auto;
		margin: 50px 0 0;
		background: #54416d;
		display: flex;
		flex-direction: column;
	`;
	return (
		<Cont id="cont" className="">
			<NamesRow>
				<IconsCont>
					<h1 class="names">Mohammad Atharbeh</h1> <br />
					<div>
						<a href={"https://www.facebook.com/VasielyGA.97/"} class="link">
							<i class="devicon-facebook-plain icon"></i>
						</a>
						<a href={"https://github.com/moevasya"} class="link">
							<i class="devicon-github-original icon"></i>
						</a>
						<a href={"https://gitlab.com/users/moevasya/projects"} class="link">
							<i class="devicon-gitlab-plain icon"></i>
						</a>
						<a
							href={
								"https://www.linkedin.com/in/mohammad-atharbeh-8a6227185/?originalSubdomain=jo"
							}
							class="link"
						>
							<i class="devicon-linkedin-plain icon"></i>
						</a>
					</div>
				</IconsCont>

				<SecondCont className="second-cont">
					<h1 class="names">Fatima Abdo</h1> <br />
					<div>
						<a href={"https://www.facebook.com/fatima.abdo.92167"} class="link">
							<i class="devicon-facebook-plain icon"></i>
						</a>
						<a href={"https://twitter.com/Ab23Fatima?s=09"} class="link">
							<i class="devicon-twitter-original icon"></i>
						</a>

						<a
							href={"https://www.linkedin.com/in/fatima-abdo-008902205"}
							class="link"
						>
							<i class="devicon-linkedin-plain icon"></i>
						</a>
					</div>
				</SecondCont>
				<IconsCont>
					<h1 class="names">Zidane Al-Adwan</h1> <br />
					<div>
						<a href={"https://www.facebook.com/zizou.aladwan"} class="link">
							<i class="devicon-facebook-plain icon"></i>
						</a>

						<a href={"https://www.linkedin.com/"} class="link">
							<i class="devicon-linkedin-plain icon"></i>
						</a>
					</div>
				</IconsCont>
			</NamesRow>
			<Subcont>
				<ListItems className="List">
					<Link to="/" onClick={scrollTop}>
						<li>Home</li>
					</Link>
					<Link to="/News0" onClick={scrollTop}>
						<li>News</li>
					</Link>
					<Link to="/WeatherAmman" onClick={scrollTop}>
						<li>Weather</li>
					</Link>
					<Link to="/Tour/Petra" onClick={scrollTop}>
						<li>Tour</li>
					</Link>
				</ListItems>
			</Subcont>
			<Subcont>
				<p className="center">
					{" "}
					Supported By NewsApi.org &copy; and OpenWeatherMapApi.org &copy;
					<br></br> design and executed by: <br /> Mohammad Atharbeh
					31801001050, <br /> Fatima Abdo 31601002035 <br />&<br /> Zidane
					Al-Adwan 31301002512{" "}
				</p>
			</Subcont>
		</Cont>
	);
}

const Subcont = styled.div`
	width: 100%;
	height: 100px;
	color: white;
	display: flex;
	flex-direction: row;
`;
const IconsCont = styled.div`
	width: 70%;
	height: auto;
	color: white;
	display: flex;
	flex-direction: column;
	text-align: center;
	padding-top: 50px;
	justify-content: center;
	flex-wrap: wrap;
`;
const ListItems = styled.ul`
	display: flex;
	list-style-type: none;
	margin: auto;
	justify-content: center;
	flex-wrap: wrap;
`;
const SecondCont = styled.div`
	width: 70%;
	height: auto;
	color: white;
	display: flex;
	flex-direction: column;
	text-align: center;
	justify-content: center;
	flex-wrap: wrap;
	padding-top: 50px;
`;

const NamesRow = styled.div`
	display: flex;
	flex-direction: row;
	width: 100%;
	height: auto;
	justify-content: space-around;
	flex-wrap: wrap;
`;
